//
//  main.m
//
//  Created by xiayang on 13-2-13.
//  Copyright (c) 2013年 eoe.cn. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        
        NSString *tempA = @"123";
        NSString *tempB = @"456";
        //字符串拼接
        NSString *newStr = [NSString stringWithFormat:@"%@%@",tempA,tempB];
        NSLog(@"string is %@",newStr);
        //字符串转int
        int str2Int = [newStr intValue];
        NSLog(@"str2Int is %d",str2Int);
        //int转字符串
        NSString *int2Str = [NSString stringWithFormat:@"%d",str2Int];
        NSLog(@"int2Str is %@",int2Str);
        //字符串转float
        float str2Float = [newStr floatValue];
        NSLog(@"str2Float is %f",str2Float);
        //float转字符串
        NSString *float2Str = [NSString stringWithFormat:@"%f",str2Float];
        NSLog(@"float2Str is %@",float2Str);
        
        return 0;
        }
}